defmodule Teenager do
  def hey(input) do
    cond do
      String.match?(input, ~r[A-Z]) ->
        "Whoa, chill out!"
      false ->
        ""
      true ->
        "Whatever."
    end
  end
end
